import { pokeColors } from "./pokeColors";

declare module '@emotion/react' {
  export interface Theme {
    colors: {
      primary: string;
      secondary: string;
      trinary: string;
      error: string;
      headerShadow: string;
    };
  }
}

export const PokeTheme = {
  colors: {
    primary: pokeColors.primary,
    secondary: pokeColors.secondary,
    trinary: pokeColors.trinary,
    error: pokeColors.error,
    headerShadow: pokeColors.headerShadow
  },
};
