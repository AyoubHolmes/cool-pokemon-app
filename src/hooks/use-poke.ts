import { getPokemons } from "apis/pokemon";
import { AxiosError } from "axios";
import { useEffect, useState } from "react";
import { PokemonRequest } from "shared/types/pokemon-request";

export const usePoke = () => {
    const [pokemons, setPokemons] = useState<any[]>([]);
    const [error,setError] = useState<AxiosError | null>(null)
    const [loading,setLoading] = useState<boolean>(false)

    const getPokemon = async () => {
        try{
            setLoading(true)
            const response: PokemonRequest = await getPokemons();
            setPokemons(response.results);
            }catch(err: any){
                setError(err)
            }finally{
                setLoading(false)
            }
    }
    useEffect(() => {
        getPokemon();
    }, []);
    return { pokemons, loading, error }
}