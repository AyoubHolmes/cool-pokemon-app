import React from "react";
import { TopBar, LogoImg, Wrapper, Card, ErrorCard } from "./HomePage.style";
import pokeballCatchIcon from "assets/icons/pokeballCatchIcon.svg";
import { usePoke } from "../hooks/use-poke";
import { CircularProgress } from "@mui/material";

const HomePage: React.FC = () => {
  const { pokemons, error, loading } = usePoke();

  return (
    <>
      <TopBar>
        <LogoImg src={pokeballCatchIcon} alt={""} />
      </TopBar>
      <Wrapper>
        { error ? 
            <ErrorCard> {error.message} </ErrorCard> :
            loading ? 
                <CircularProgress color="inherit" /> :
                pokemons.map((pokemon, index) => (<Card key={index} />))
        }
      </Wrapper>
    </>
  );
};

export default HomePage;
