import styled from '@emotion/styled'

export const TopBar = styled.div`
    width: 100%;
    height: 4rem;
    background-color: ${(props) => props.theme.colors.primary};
    display: flex;
    justify-content: center;
    box-shadow: 0 0.4rem 1rem 0 ${(props) => props.theme.colors.headerShadow};
`;

export const LogoImg = styled.img`
    height: 3.5rem;
`;

export const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    padding: 2rem;
`;

export const ErrorCard = styled.div`
    width: 100%;
    height: 3rem;
    background-color: ${(props) => props.theme.colors.error};
    color: ${(props) => props.theme.colors.trinary};
    padding: 2rem;
    border-radius: 1.2rem;
    text-align: center;
`

export const Card = styled.div`
    margin: 1rem;
    width: 20rem;
    height: 20rem;
    background-color: ${(props) => props.theme.colors.trinary};
    border-radius: 1.5rem;
    box-shadow: 0px 0.5rem 0px 0px  ${(props) => props.theme.colors.secondary};
`;
