export interface PokemonRequest {
    count: number;
    next: string;
    previous?: string | null;
    results: any[]
};