import React from 'react';
import {ThemeProvider as EmotionProvider} from '@emotion/react';
import { PokeTheme } from 'theme/pokeTheme';
import { HomePage } from 'HomePage';


const App: React.FC = () => {
  return (
    <EmotionProvider theme={PokeTheme}>
      <HomePage />
    </EmotionProvider>
  );
}

export default App;
