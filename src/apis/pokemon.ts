import { PokeClient } from "./http-client";

const POKEMON_URL = '/pokemon';

export const getPokemons = async () => {
    return  await PokeClient.get(POKEMON_URL);
  };